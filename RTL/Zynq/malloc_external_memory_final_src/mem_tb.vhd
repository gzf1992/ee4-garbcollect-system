
------------------------------------------------------------------
--Testbench for generic memory

--vhdl test entity: memory
--author: Zifan Guo
--version: 27/05/2015
------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

entity mem_tb is
end mem_tb;

architecture tb of mem_tb is
	signal clk,rst,en					:std_logic;
	signal A_addr,B_addr				:std_logic_vector(31 downto 0);
	signal A_din,B_din,A_dout,B_dout	:std_logic_vector(31 downto 0);
	signal W_A,W_B						:std_logic_vector(3 downto 0);
	
component base_sys_malloc_sep_ram_0_bram_0 is
  port (
    clka : in STD_LOGIC;
    rsta : in STD_LOGIC;
    ena : in STD_LOGIC;
    wea : in STD_LOGIC_VECTOR ( 3 downto 0 );
    addra : in STD_LOGIC_VECTOR ( 31 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    douta : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clkb : in STD_LOGIC;
    rstb : in STD_LOGIC;
    enb : in STD_LOGIC;
    web : in STD_LOGIC_VECTOR ( 3 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 31 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 31 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  end component base_sys_malloc_sep_ram_0_bram_0;
	
begin	

--clock generation process	
clk_gen:process
begin
	clk<='0';
	wait for 50 ns;
	clk<='1';
	wait for 50 ns;
end process clk_gen;

--test entity
m1:component base_sys_malloc_sep_ram_0_bram_0
	port map(
	clka =>clk,
	addra =>A_addr,
	dina =>A_din,
	douta =>A_dout,
	ena =>en,
	rsta => rst,
	wea => W_A,
	
	clkb =>clk,
	addrb =>B_addr,
	dinb =>B_din,
	doutb =>B_dout,
	enb =>en,
	rstb => rst,
	web => W_B
	);


main: process 	
begin
	en<='1';
	rst<='1';
	wait until clk'event and clk = '1';
	rst<='0';
	A_addr<=(others=>'0');
	B_addr<=(2=>'1',others=>'0');
	wait until clk'event and clk = '1';
	W_A<="1111";
	W_B<="1111";
	A_din<="00000000000000100000011111111110";
	B_din<="00000000000000100000000000001000";
	wait until clk'event and clk = '1';
	A_addr<=(0=>'1',others=>'0');
	B_addr<=(1=>'1',others=>'0');
	W_A<="0000";
	W_B<="0000";
	wait until clk'event and clk = '1';
	A_addr<=(others=>'1');
	wait until clk'event and clk = '1';
	wait until clk'event and clk = '1';
	wait until clk'event and clk = '1';
	REPORT "test finished normally." SEVERITY failure;
end process main; 

end tb;
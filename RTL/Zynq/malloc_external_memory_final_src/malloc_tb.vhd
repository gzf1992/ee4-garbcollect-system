------------------------------------------------------------------
--Testbench for malloc blcok
--reads command.txt for input data

--vhdl test entity: malloc
--author: Zifan Guo
--version: 23/02/2014
------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE std.textio.ALL;
USE work.txt_util.ALL;
USE work.config_pak.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

entity malloc_tb is
end malloc_tb;

architecture tb of malloc_tb is

--signals
	signal clk,rst,en,operation,done_i,request	:std_logic;
	signal size_in,f_addr_i 					:std_logic_vector(15 downto 0);
	signal result								:std_logic_vector(31 downto 0);
	signal A_addr,B_addr						:std_logic_vector(31 downto 0);
	signal A_din,B_din,A_dout,B_dout			:std_logic_vector(31 downto 0);
	signal W_A,W_B								:std_logic_vector(3 downto 0);

--component
component base_sys_malloc_sep_ram_0_bram_0 is
  port (
    clka : in STD_LOGIC;
    rsta : in STD_LOGIC;
    ena : in STD_LOGIC;
    wea : in STD_LOGIC_VECTOR ( 3 downto 0 );
    addra : in STD_LOGIC_VECTOR ( 31 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 31 downto 0 );
    douta : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clkb : in STD_LOGIC;
    rstb : in STD_LOGIC;
    enb : in STD_LOGIC;
    web : in STD_LOGIC_VECTOR ( 3 downto 0 );
    addrb : in STD_LOGIC_VECTOR ( 31 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 31 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
end component base_sys_malloc_sep_ram_0_bram_0;
  
begin	
	
--clock generation process	
clk_gen:process
begin
	clk<='0';
	wait for 50 ns;
	clk<='1';
	wait for 50 ns;
end process clk_gen;

m1:component base_sys_malloc_sep_ram_0_bram_0
	port map(
	clka =>clk,
	addra =>A_addr,
	dina =>A_din,
	douta =>A_dout,
	ena =>en,
	rsta => rst,
	wea => W_A,
	
	clkb =>clk,
	addrb =>B_addr,
	dinb =>B_din,
	doutb =>B_dout,
	enb =>en,
	rstb => rst,
	web => W_B
	);
	
--test entity
malloc:entity work.malloc
	port map(
		clk 	=>clk ,
		rst		=>rst,
		en		=>en,		
		go		=>request,
		op 		=>operation,
		m_size 	=>size_in,
		f_addr	=>f_addr_i,
		m_addr 	=>result,
		done	=>done_i,
		addra	=>A_addr,
		addrb	=>B_addr,
		dina	=>A_din,
		dinb	=>B_din,
		douta	=>A_dout,
		doutb	=>B_dout,
		wea		=>W_A,
		web		=>W_B
	);


main: process 	
	file f 				:TEXT open read_mode is "command.txt";
	variable buf 		:LINE;
	variable n			:integer;--line counter
	variable incorrect_result : integer;
	
	variable op				:bit;
	variable size			:bit_vector(15 downto 0);
	variable free_addr		:bit_vector(15 downto 0);
	variable nominal_result :bit_vector(15 downto 0);
begin
	rst<='1';
	request<='0';
	wait until clk'event and clk = '1';
	rst <= '0';
	en<='1';
	
	n:=1;
	incorrect_result:=0;
	--------------------------------------------
	--read line
	
while not endfile(f) loop
	wait until clk'event and clk='1';
		readline(f,buf);
		If buf'LENGTH = 0 THEN
			REPORT "skipping line: " & INTEGER'IMAGE(n) SEVERITY note;
		ELSE
			REPORT "Reading input line:" & INTEGER'IMAGE(n) SEVERITY note;
				request<='1';
				read(buf, op);
				operation<=b2l(op);
				
				read(buf,size);
				size_in<=to_stdlogicvector(size);
				
				read(buf,free_addr);
				f_addr_i<=to_stdlogicvector(free_addr);

				read(buf,nominal_result);
							
			REPORT "Nominal: " & str(to_stdlogicvector(nominal_result)) & "calculated " & str(result) SEVERITY note;
		END IF;
		wait until done_i'event and done_i='1';
		wait until clk'event and clk='1';
		wait until clk'event and clk='1';
		wait until clk'event and clk='1';
		request<='0';
end loop;
		wait until clk'event and clk='1';
		wait until clk'event and clk='1';
		wait until clk'event and clk='1';

REPORT "test finished normally." SEVERITY failure;
end process main; 

end tb;
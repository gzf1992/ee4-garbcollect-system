/*
 * testing.c
 *
 *  Created on: 26 May 2015
 *      Author: Zifan
 */


#include "malloc_sep_ram.h"
#include "xparameters.h"

#define MALLOC_BASE XPAR_MALLOC_SEP_RAM_0_S00_AXI_BASEADDR
#define NDONE 0x000

void system_Reset(void);
u32 malloc(int size);
void free(int free_address);

int main(void)
{	u32 x;

	system_Reset();

	xil_printf("malloc/free test begin\r\n");
	xil_printf("----------------------------------\r\n\n");
	x=malloc(10);
	x=malloc(10);
	x=malloc(20);
	x=malloc(20);
	x=malloc(20);
	x=malloc(7000);
	x=malloc(10);

	free(28);
	free(76);
	free(48);
	free(104);
	free(132);
	free(7140);
	free(8);

	malloc(50);

	return 1;
}

void system_Reset(void)
{
	MALLOC_SEP_RAM_mWriteReg(MALLOC_BASE,4,0x00000004);	//enable for memory
	MALLOC_SEP_RAM_mWriteReg(MALLOC_BASE,4,0x0000000C);	//reset memory
	MALLOC_SEP_RAM_mWriteReg(MALLOC_BASE,4,0x00000000); //disable for memory
}

u32 malloc(int size)
{
	u32 result=0;
	u32 malloc_size=size;

	while (MALLOC_SEP_RAM_mReadReg(MALLOC_BASE,12)==0x00000001);	//check if done=0
	MALLOC_SEP_RAM_mWriteReg(MALLOC_BASE,0,malloc_size);			//malloc_size
	MALLOC_SEP_RAM_mWriteReg(MALLOC_BASE,4,0x00000006);				//enable+go+operation
	while (MALLOC_SEP_RAM_mReadReg(MALLOC_BASE,12)==0x00000000);	//wait until is done	(hardware set done to 1)
	result=MALLOC_SEP_RAM_mReadReg(MALLOC_BASE,8);
	MALLOC_SEP_RAM_mWriteReg(MALLOC_BASE,4,0x00000000);				//disable and set go to 0

	xil_printf("MALLOC address: %d\r\n", result);
	return result;
}

void free(int free_address)
{
	u32 f_addr=(free_address<<16);
	while (MALLOC_SEP_RAM_mReadReg(MALLOC_BASE,12)==0x00000001);	//check if done=0
	MALLOC_SEP_RAM_mWriteReg(MALLOC_BASE,0,f_addr);					//free_addr
	MALLOC_SEP_RAM_mWriteReg(MALLOC_BASE,4,0x00000007);				//enable+GO+operation
	while (MALLOC_SEP_RAM_mReadReg(MALLOC_BASE,12)==0x00000000);	//wait until is done
	xil_printf("address %d freed\r\n",(f_addr>>16));
	MALLOC_SEP_RAM_mWriteReg(MALLOC_BASE,4,0x00000000);				//disable and set go to 0
}

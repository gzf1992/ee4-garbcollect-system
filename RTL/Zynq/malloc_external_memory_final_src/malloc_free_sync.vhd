library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.config_pak.all;
use work.all;

entity malloc is
	generic
	(
		BRAM_depth: integer	:=2048
	);
	port (
		clk 	: in  std_logic;								--clock
		rst		: in  std_logic;								--reset
		en  	: in  std_logic;								--block enable
		go  	: in  std_logic;								--GO
		op  	: in  std_logic;								--operation mode(malloc/free)
		m_size	: in  std_logic_vector(15 downto 0);			--malloc size !!!!can be merged with free address
		f_addr	: in  std_logic_vector(15 downto 0);			--free address
		m_addr	: out std_logic_vector(31 downto 0);			--malloc address
		done	: out std_logic;
		--*************************mem interface*******************************
		addra	: out std_logic_vector(31 downto 0);
		addrb	: out std_logic_vector(31 downto 0);
		dina	: out std_logic_vector(31 downto 0);
		dinb	: out std_logic_vector(31 downto 0);
		douta	: in std_logic_vector(31 downto 0);
		doutb	: in std_logic_vector(31 downto 0);
		wea		: out std_logic_vector(3 downto 0);
		web		: out std_logic_vector(3 downto 0)
	);
end entity malloc;

architecture rtl of malloc is

signal cur_search_ptr					:	header;				--pointers

signal nstate,nnstate					: 	malloc_state_t;     --state signals

--control signals
signal hfront,hend						:	std_logic;
signal start,go_delayed					:	std_logic;

--I/O signals
signal s_size							:	size_t;
signal s_f_addr							:	integer;

--memory signals
signal A_addr,B_addr					:	slv(31 downto 0);

begin

--Input 
s_f_addr<=to_integer(unsigned(f_addr));

rounder:process(m_size)
begin
if m_size(1 downto 0)="00" then
s_size<=unsigned(m_size);
else
s_size<=unsigned(m_size(15 downto 2)&"00")+4;
end if;
end process;

go_edge_detector:process(clk,rst)
begin
	if rst='1' then
		start<='0';
		go_delayed<='0';
	elsif rising_edge(clk) then
		go_delayed<=go;
		start<=not go_delayed and go;
	end if;
end process;

--for registering addra&addrb
addra<=A_addr;
addrb<=B_addr;

--control signals
	hfront<='1' when cur_search_ptr.h_prev=prev_err else '0';		--indication of reaching list front
	hend<='1' when cur_search_ptr.h_next=next_err else '0';
	
--FSM
FSM_proc:process
variable ptr					:	header;
variable new_header				:	slv(63 downto 0);
variable free_sel				:	slv(1 downto 0);
variable prev_size,next_size	:	size_t;

begin
wait until clk'EVENT and clk='1';

if	rst='1' then 
	nstate<=mem_wait1;
	nnstate<=idle;
	
	done<='0';
	m_addr<=(others=>'0');
	
	wea		<="1111";
	web		<="1111";
	A_addr	<=i2m(0);
	B_addr	<=i2m(addr_step);
	dina	<=slv(i2t(2048*4-7)&i2t(header_size));
	dinb	<=slv(i2t(next_err)&i2t(prev_err));
	
else
	case(nstate)is
	----wait 2 cycles
	when mem_wait1=> nstate<=mem_wait2;

	when mem_wait2=> nstate<=nnstate;

	when idle=>	if go='0' then
					done<='0';
				end if;
				
				wea		<="0000";
				web		<="0000";
				if en = '1' and  start='1' then
					if op='0' then
					A_addr<=i2m(0);
					B_addr<=i2m(addr_step);
					nstate<=mem_wait1;
					nnstate<=find;
					else
					A_addr<=i2m(s_f_addr-header_size);
					B_addr<=i2m(s_f_addr-header_size+addr_step);
					nstate<=mem_wait1;
					nnstate<=init_free;
					end if;
				end if;
	when find=>	
				ptr:=readHeader(douta,doutb);
				
				cur_search_ptr<=ptr;
				
					if 	(ptr.free='1' and (unsigned(ptr.size)>=s_size)) then
						if (unsigned(ptr.size)<=s_size+header_size) then 		--just fit
							wea<="0100";
							web<="0000";
							
							dina<=douta(31 downto 17)&'0'&douta(15 downto 0);
							
							done<='1';
							nstate<=mem_wait1;	
							nnstate<=idle;
						else
							wea<="1100";						--write
							web<="1100";
							
							dina<=	slv(s_size)&douta(15 downto 0);					--size
							dinb<=	slv(i2t(ptr.data)+s_size)&doutb(15 downto 0);	--next
							
							nstate<=mem_wait1;
							nnstate<=alloc;
						end if;
						m_addr<=norm_out&std_logic_vector(i2t(ptr.data));
						
					else
						wea		<="0000";
						web		<="0000";
						if ptr.h_next/=next_err then
							A_addr<=i2m(ptr.h_next);			--next block
							B_addr<=i2m(ptr.h_next+addr_step);
							nstate<=mem_wait1;
							nnstate<=find;
						else							--error handle
							m_addr<=error_out;
							nstate<=mem_wait1;
							nnstate<=idle;
							done<='1';
						end if;
					end if;				
	when alloc=>	new_header:=create_newHeader(cur_search_ptr,s_size);
					
					wea<="1111";
					web<="1111";
					A_addr<=i2m(to_integer(s_size+ptr.data));
					B_addr<=i2m(to_integer(s_size+ptr.data+addr_step));
					
					dina<=new_header(63 downto 32);
					dinb<=new_header(31 downto 0);
					
					done<='1';
					nstate<=mem_wait1;
					nnstate<=idle;
	when init_free=>
					wea		<="0000";
					web		<="0000";
					ptr:=readHeader(douta,doutb);
					cur_search_ptr<=ptr;
					if (ptr.h_prev/=prev_err) then
					A_addr<=i2m(ptr.h_prev);
					end if;
					if (ptr.h_next/=next_err) then
					B_addr<=i2m(ptr.h_next);
					end if;
					nstate<=mem_wait1;
					nnstate<=free;
	------------------------------------------------------------------------------------------------------
	----freesel 		case								strategy
	----	00			notfree->free.->notfree				free current addr.
	----	01			notfree->free.->free				free current addr.+link current to Nnext
	----	10			free->free.->notfree				link prev to next
	----	11			free->free.->free					link prev to Nnext
	------------------------------------------------------------------------------------------------------				
	when free=>		
				prev_size:=unsigned(douta(31 downto 17)&'0');
				next_size:=unsigned(doutb(31 downto 17)&'0');
			
				if hfront='1' then
						free_sel:='0'&doutb(16);
				else
					if 	hend='1' then
						free_sel:=douta(16)&'0';
					else
						free_sel:=douta(16)&doutb(16);
					end if;
				end if;
					
						if free_sel="00" then											--notfree->free->notfree
							wea<="0100";
							A_addr<=i2m(cur_search_ptr.data-header_size);
							dina<=std_logic_vector((cur_search_ptr.size+1))&douta(15 downto 0);
							web	<="0000";
							done<='1';
							nstate<=mem_wait1;
							nnstate<=idle;
						
						elsif free_sel="01" then										--notfree->free->free
							--change current node size&status
							wea<="1100";
							A_addr<=i2m(cur_search_ptr.data-header_size);
							dina<=std_logic_vector((cur_search_ptr.size+1+header_size+next_size))&douta(15 downto 0);
							--check address after next
							web		<="0000";
							B_addr<=i2m(cur_search_ptr.h_next+addr_step);
							
							nstate<=mem_wait1;
							nnstate<=link;
						
						elsif free_sel="10" then							--free->free->notfree 	
							--change previous node size&status
							wea<="1100";
							A_addr<=A_addr;
							dina<=std_logic_vector((cur_search_ptr.size+1+header_size+prev_size))&douta(15 downto 0);
							web	<="0000";
							B_addr<=i2m(cur_search_ptr.data-header_size+addr_step);
							nstate<=mem_wait1;
							nnstate<=link;
						
						else														--free->free->free 		
							wea<="1100";
							A_addr<=i2m(cur_search_ptr.h_prev);
							dina<=std_logic_vector((cur_search_ptr.size+1+header_size+header_size+prev_size+next_size))&douta(15 downto 0);
							web<="0000";
							B_addr<=i2m(cur_search_ptr.h_next+addr_step);
							nstate<=mem_wait1;
							nnstate<=link;
						end if;
	when link=>									
					wea<="1100";
					A_addr<=slv(unsigned(A_addr)+addr_step);
					dina<=doutb(31 downto 16)&douta(15 downto 0);		--link forward
					
					if (doutb(31 downto 16)/=slv(i2t(next_err))) then
					web<="0011";
					dinb<=doutb(31 downto 16)&A_addr(15 downto 0);		--link back
					B_addr<=i2m(to_integer(unsigned(doutb(31 downto 16)))+addr_step);
					else
					web		<="0000";
					end if;
					
					done<='1';
					nstate<=mem_wait1;
					nnstate<=idle;
	end case;
end if;
end process FSM_proc;

end architecture rtl;
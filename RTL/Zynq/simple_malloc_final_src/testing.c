#include "malloc.h"
#include "xparameters.h"

#define MALLOC_BASE XPAR_MALLOC_0_S00_AXI_BASEADDR
#define NDONE 0x000

void system_Reset(void);
u32 malloc(int size);
void free(int free_address);

int main(void)
{	u32 x;

	xil_printf("malloc/free test begin\r\n");
	xil_printf("----------------------------------\r\n\n");
	x=malloc(10);
	x=malloc(20);
	x=malloc(20);
	x=malloc(20);
/*	free(8);
	free(28);
	free(34);
	free(40);
*/
	return 1;
}

u32 malloc(int size)
{
	u32 result=0;
	u32 malloc_size=size;
	while (MALLOC_mReadReg(MALLOC_BASE,12)==0x00000001);	//check if done=0
	MALLOC_mWriteReg(MALLOC_BASE,0,malloc_size);			//malloc_size
	MALLOC_mWriteReg(MALLOC_BASE,4,0x00000006);				//enable+go+operation
	while (MALLOC_mReadReg(MALLOC_BASE,12)==0x00000000);	//wait until is done	(hardware set done to 1)
	result=MALLOC_mReadReg(MALLOC_BASE,8);
	MALLOC_mWriteReg(MALLOC_BASE,4,0x00000000);				//set go to 0

	xil_printf("MALLOC address: %d\r\n", result);
	return result;
}

void free(int free_address)
{
	u32 f_addr=free_address;
	MALLOC_mWriteReg(MALLOC_BASE,12,0x00000000);		//reset to not done
	MALLOC_mWriteReg(MALLOC_BASE,0,f_addr);				//free_addr
	MALLOC_mWriteReg(MALLOC_BASE,4,0x00000007);			//enable+GO+operation
	while (MALLOC_mReadReg(MALLOC_BASE,12)==0);		//wait until is done
	MALLOC_mWriteReg(MALLOC_BASE,4,0x00000000);			//stop operation
	xil_printf("address %d freed",free_address);
}

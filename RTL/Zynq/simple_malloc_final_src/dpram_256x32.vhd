library ieee;
use ieee.std_logic_1164.all;

entity dpram_single_clk_256x32 is
generic
(
DATA_WIDTH : natural := 32;
ADDR_WIDTH : natural := 16	--only 8 bits in use
);
port
(
	clk : in std_logic;
	addr_a: in natural range 0 to 2**ADDR_WIDTH - 1;
	addr_b: in natural range 0 to 2**ADDR_WIDTH - 1;
	data_a: in std_logic_vector((DATA_WIDTH-1) downto 0);
	data_b: in std_logic_vector((DATA_WIDTH-1) downto 0);
	we_a: in std_logic := '1';
	we_b: in std_logic := '1';
	q_a : out std_logic_vector((DATA_WIDTH -1) downto 0);
	q_b : out std_logic_vector((DATA_WIDTH -1) downto 0)
);
end dpram_single_clk_256x32;

architecture rtl of dpram_single_clk_256x32 is
	
-- Build a 2-D array type for the RAM
subtype word_t is std_logic_vector((DATA_WIDTH-1) downto 0);
type memory_t is array(0 to 255) of word_t;

-- Declare the RAM signal.
shared variable ram : memory_t:=(0=>x"00ff0002",1=>x"000000ff",others=>x"00000000");

begin

p1:process
	begin
	wait until clk'EVENT and clk='1';
	if(we_a = '1') then
		ram(addr_a) := data_a;
		-- Read-during-write on the same port returns NEW data
		q_a <= data_a;
	else
		-- Read-during-write on the mixed port returns OLD data
		q_a <= ram(addr_a);
	end if;
end process p1;

p2:process
begin
	wait until clk'EVENT and clk='1';
			
		if(we_b = '1') then
		ram(addr_b) := data_b;
		-- Read-during-write on the same port returns NEW data
		q_b <= data_b;
		else
		-- Read-during-write on the mixed port returns OLD data
		q_b <= ram(addr_b);
	end if;
	
end process p2;

end rtl;
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.all;

package config_pak is

--alias
ALIAS slv is std_logic_vector;
ALIAS usg is unsigned;
ALIAS sgn is signed;

--types
subtype addr_t is integer range 0 to 2**16-1;
subtype size_t is usg(15 downto 0);
subtype data_t is slv(31 downto 0);

--header
type headerHi is record	--Hi
	size:	size_t;
	free:	std_logic;
	h_next: addr_t;
end record;
type headerLo is record --Lo
	data:	addr_t;
	h_prev: addr_t;		
end record;

--constants
constant header_size: 	integer:= 8;
constant addr_step:		integer:= 4;
constant next_err:		integer:= 0;
constant prev_err:		integer:= 2**16-1;
constant norm_out		:slv(15 downto 0):=(others=>'0');
constant error_out		:slv(31 downto 0):=(others=>'1');

--states
type malloc_state_t is (mem_wait1,mem_wait2,idle,find,alloc1,alloc2,setup_init_free,init_free0,init_free1,free,link);

--utility functions
FUNCTION readHeaderHi(A:slv(31 downto 0)) RETURN headerHi;
FUNCTION readHeaderLo(B:slv(31 downto 0)) RETURN headerLo;

FUNCTION create_newHeaderHi(prev_ptrHi:headerHi;size:size_t) RETURN std_logic_vector;
FUNCTION create_newHeaderLo(size:size_t;prevHi_addr:usg(15 downto 0)) RETURN std_logic_vector;
FUNCTION update_oldHeaderHi(size:size_t;curHi_addr:usg(15 downto 0)) RETURN std_logic_vector;

--type_conversion functions
FUNCTION b2l(b:BIT) return std_logic;	
FUNCTION i2t(i : integer) RETURN usg;
FUNCTION i2m(i : integer) RETURN slv;

end package config_pak;


package body config_pak is
-----------------------------------------------------------------------------------------------------------
FUNCTION readHeaderHi(A:slv(31 downto 0)) RETURN headerHi IS
		VARIABLE curPtr:headerHi;
	BEGIN
		curPtr.size:=unsigned(A(31 downto 17)&'0');
		curPtr.free:=A(16);
		curPtr.h_next:=to_integer(unsigned(A(15 downto 0)));
		return(curPtr);
	END;
FUNCTION readHeaderLo(B:slv(31 downto 0)) RETURN headerLo IS
		VARIABLE curPtr:headerLo;
	BEGIN
		curPtr.data:=to_integer(unsigned(B(31 downto 16)));
		curPtr.h_prev:=to_integer(unsigned(B(15 downto 0)));
		return(curPtr);
	END;
-----------------------------------------------------------------------------------------------------------

FUNCTION create_newHeaderHi(prev_ptrHi:headerHi;size:size_t) RETURN std_logic_vector IS
	VARIABLE out_mem:std_logic_vector(31 downto 0);
BEGIN
	out_mem(31 downto 16):= std_logic_vector(prev_ptrHi.size-size-header_size+1);	--size+free
	out_mem(15 downto 0) := std_logic_vector(i2t(prev_ptrHi.h_next));				--h_next
	return out_mem;
END;
FUNCTION create_newHeaderLo(size:size_t;prevHi_addr:usg(15 downto 0)) RETURN std_logic_vector IS
	VARIABLE out_mem:std_logic_vector(31 downto 0);
BEGIN
	out_mem(31 downto 16):= std_logic_vector(prevHi_addr+header_size+size+header_size);	--data
	out_mem(15 downto 0) := std_logic_vector(prevHi_addr);								--h_prev
	return out_mem;
END;
-----------------------------------------------------------------------------------------------------------
FUNCTION update_oldHeaderHi(size:size_t;curHi_addr:usg(15 downto 0)) RETURN std_logic_vector IS
	VARIABLE out_mem:std_logic_vector(31 downto 0);
BEGIN
	out_mem(31 downto 16):= std_logic_vector(size);
	out_mem(15 downto 0) := std_logic_vector(curHi_addr+size+header_size);
	return out_mem;
END;
--oldHeaderLo is the same
-----------------------------------------------------------------------------------------------------------
FUNCTION b2l(b : BIT) RETURN std_logic IS
	BEGIN
		IF b = '0' THEN
			RETURN '0';
		END IF;
		RETURN '1';
	END FUNCTION;

FUNCTION i2t(i : integer) RETURN usg IS
	BEGIN
	return(to_unsigned(i,16));
	END FUNCTION;

FUNCTION i2m(i : integer) RETURN slv IS
	BEGIN
	return(slv(to_unsigned(i,32)));
	END FUNCTION;
	
end package body config_pak;

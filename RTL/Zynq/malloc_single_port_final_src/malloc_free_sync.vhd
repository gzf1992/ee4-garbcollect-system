library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.config_pak.all;
use work.all;

entity malloc is
	generic 
	(
		BRAM_depth: integer:=2048
	);
	port (
		clk 	: in  std_logic;								--clock
		rst		: in  std_logic;								--rst
		en  	: in  std_logic;								--block enable
		go  	: in  std_logic;								--GO
		op  	: in  std_logic;								--operation mode(malloc/free)
		mf_in	: in  std_logic_vector(31 downto 0);			--malloc size--free address
		m_addr	: out std_logic_vector(31 downto 0);			--malloc address
		done	: out std_logic;
		--*************************mem interface*******************************
		addr	: out std_logic_vector(31 downto 0);
		din		: out std_logic_vector(31 downto 0);
		dout	: in std_logic_vector(31 downto 0);
		we		: out std_logic_vector(3 downto 0)
	);
end entity malloc;

architecture rtl of malloc is

signal cur_search_ptrHi,next_search_ptrHi		:	headerHi;				--pointers
signal cur_search_ptrLo					:	headerLo;				--pointers

signal nstate,nnstate					: 	malloc_state_t;     --state signals

--control signals
signal hfront,hend						:	std_logic;
signal start,go_delayed						:	std_logic;
--I/O signals
signal s_size							:	size_t;
signal s_f_addr							:	integer;

--memory control signals
signal mem_addr							:	usg(15 downto 0);
signal A_addr							:	usg(15 downto 0);

begin

--Input 

s_f_addr<=to_integer(resize(unsigned(mf_in),16)-header_size);

rounder:process(mf_in)	--round up to multiples of 4
begin
if mf_in(1 downto 0)="00" then
s_size<=resize(unsigned(mf_in),16);
else
s_size<=resize(unsigned(mf_in(31 downto 2)&"00"),16)+4;
end if;
end process;

go_edge_detector:process(clk,rst)
begin
	if rst='1' then
		start<='0';
		go_delayed<='0';
	elsif rising_edge(clk) then
		go_delayed<=go;
		start<=not go_delayed and go;
	end if;
end process;

--for registering addr
addr<="0000000000000000"&slv(mem_addr);

--control signals
	hfront<='1' when cur_search_ptrLo.h_prev=prev_err else '0';		--indication of reaching list front
	hend<='1' when cur_search_ptrHi.h_next=next_err else '0';
	
--FSM
FSM_proc:process
variable ptrHi											:	headerHi;
variable ptrLo											:	headerLo;
variable free_sel										:	slv(1 downto 0);
variable old_header_Hi									:	slv(31 downto 0);
variable next_size,prev_size							:	size_t;
begin
wait until clk'EVENT and clk='1';

if	rst='1' then 
	nstate<=idle;
	done<='0';
	m_addr<=(others=>'0');
	we<="0000";
	
else
	case(nstate)is
	----wait 2 cycles
	when mem_wait1=> nstate<=mem_wait2;

	when mem_wait2=> nstate<=nnstate;

	when idle=>	if go='0' then
					done<='0';
				end if;
				
				we	<="0000";
				
				if en = '1' and  start='1' then
				
					if op='0' then
					mem_addr<=i2t(0);
					nstate<=mem_wait1;
					nnstate<=find;
					else
					mem_addr<=i2t(s_f_addr);
					nstate<=mem_wait1;
					nnstate<=setup_init_free;
					end if;
				end if;
	when find => ptrHi:=readHeaderHi(dout);									--read only size|free and h_next read
				 old_header_Hi:=update_oldHeaderHi(s_size,mem_addr);		--old_header_Hi calculation
				 
				 cur_search_ptrHi<=ptrHi;
	
					if 	(ptrHi.free='1' and (unsigned(ptrHi.size)>=s_size)) then
					
						if (unsigned(ptrHi.size)<=s_size+header_size) then 			--just fit
							we<="0100";
							din<=dout(31 downto 17)&'0'&dout(15 downto 0);
						
							done<='1';
							nstate<=mem_wait1;	
							nnstate<=idle;
						else
							we<="1111";												--update_oldptr
							din<=old_header_Hi;
							
							nstate<=mem_wait1;	
							nnstate<=alloc1;
						end if;
						m_addr<=norm_out&slv(mem_addr+header_size);
					else
						we<="0000";
						if ptrHi.h_next/=next_err then
							mem_addr<=i2t(ptrHi.h_next);			--next block
							nstate<=mem_wait1;
							nnstate<=find;
						else									--error handle
							m_addr<=error_out;
							nstate<=mem_wait1;
							nnstate<=idle;
							done<='1';
						end if;
					end if;
	when alloc1=>	
					we<="1111";														--data&link back
					mem_addr<=mem_addr+s_size+header_size+addr_step;				--LO
					din<=create_newHeaderLo(s_size,mem_addr);
					
					nstate<=mem_wait1;
					nnstate<=alloc2;
	when alloc2=>	
					we<="1111";														--size&link forward
					mem_addr<=mem_addr-addr_step;											--HI					
					din<=create_newHeaderHi(cur_search_ptrHi,s_size);
					
					done<='1';
					nstate<=mem_wait1;
					nnstate<=idle;
					
	when setup_init_free=>
					cur_search_ptrHi<=readHeaderHi(dout);
					
					we<="0000";
					mem_addr<=mem_addr+addr_step;
					
					nstate<=mem_wait1;
					nnstate<=init_free0;
	when init_free0=>
					cur_search_ptrLo<=readHeaderLo(dout);
					
					we<="0000";
					if hend='0' then 
					mem_addr<=i2t(cur_search_ptrHi.h_next);
					end if;
					
					nstate<=mem_wait1;
					nnstate<=init_free1;
	when init_free1=>
					ptrHi:=readHeaderHi(dout);					
					next_search_ptrHi<=ptrHi;
					
					we<="0000";
					if hfront='0' then
					mem_addr<=i2t(cur_search_ptrLo.h_prev);
					end if;
	
					nstate<=mem_wait1;
					nnstate<=free;

	when free=>	
					ptrHi:=readHeaderHi(dout);

					if hfront='1' then
						free_sel:='0'&next_search_ptrHi.free;
					else
						if 	hend='1' then
							free_sel:=ptrHi.free&'0';
						else
							free_sel:=ptrHi.free&next_search_ptrHi.free;
						end if;
					end if;
					
					next_size:=next_search_ptrHi.size;
					prev_size:=ptrHi.size;
	------------------------------------------------------------------------------------------------------
	----freesel 		case								strategy
	----	00			notfree->free.->notfree				free current addr.
	----	01			notfree->free.->free				free current addr.+link current to Nnext
	----	10			free->free.->notfree				link prev to next
	----	11			free->free.->free					link prev to Nnext
	------------------------------------------------------------------------------------------------------					
					if free_sel="00" then											--notfree->free->notfree							
							we<="0100";
							mem_addr<=i2t(cur_search_ptrLo.data-header_size);
							din<=slv(cur_search_ptrHi.size+1)&dout(15 downto 0);
							done<='1';
							nstate<=mem_wait1;
							nnstate<=idle;
							
					elsif free_sel="01" then										--notfree->free->free
							--change current node size&status
							we<="1111";
							mem_addr<=i2t(cur_search_ptrLo.data-header_size);
							din<=slv(cur_search_ptrHi.size+1+header_size+next_size)&slv(i2t(next_search_ptrHi.h_next));
							
							if (next_search_ptrHi.h_next/=next_err) then
							A_addr<=i2t(next_search_ptrHi.h_next+addr_step);
							nstate<=mem_wait1;
							nnstate<=link;
							else
							done<='1';
							nstate<=mem_wait1;
							nnstate<=idle;
							end if;
							
					elsif free_sel="10" then							--free->free->notfree 	
							--change previous node size&status
							we<="1111";
							mem_addr<=i2t(cur_search_ptrLo.h_prev);
							din <=slv(cur_search_ptrHi.size+1+header_size+prev_size)&slv(i2t(cur_search_ptrHi.h_next));
							
							if (cur_search_ptrHi.h_next/=next_err) then
							A_addr<=i2t(cur_search_ptrHi.h_next+addr_step);
							nstate<=mem_wait1;
							nnstate<=link;
							else
							done<='1';
							nstate<=mem_wait1;
							nnstate<=idle;
							end if;
					else														--free->free->free 		
							we<="1111";
							mem_addr<=i2t(cur_search_ptrLo.h_prev);
							din<=slv(cur_search_ptrHi.size+1+header_size+header_size+prev_size+next_size)&slv(i2t(next_search_ptrHi.h_next));
							
							if (next_search_ptrHi.h_next/=next_err) then
							A_addr<=i2t(next_search_ptrHi.h_next+addr_step);
							nstate<=mem_wait1;
							nnstate<=link;
							else
							done<='1';
							nstate<=mem_wait1;
							nnstate<=idle;
							end if;
					end if;
					
	when link=>		
					
					we<="0011";
					mem_addr<=A_addr;
					din<=dout(31 downto 16)&slv(mem_addr);
					
					done<='1';
					nstate<=mem_wait1;
					nnstate<=idle;
					
	end case;
end if;
end process FSM_proc;

end architecture rtl;
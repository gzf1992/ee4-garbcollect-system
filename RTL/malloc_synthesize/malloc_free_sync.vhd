library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.config_pak.all;
use work.all;

entity malloc is
	port (
		clk 	: in  std_logic;								--clock
		en  	: in  std_logic;								--block enable
		go  	: in  std_logic;								--GO
		op  	: in  std_logic;								--operation mode(malloc/free)
		m_size	: in  std_logic_vector(15 downto 0);			--malloc size !!!!can be merged with free address
		f_addr	: in  std_logic_vector(15 downto 0);			--free address
		m_addr	: out std_logic_vector(15 downto 0);			--malloc address
		done	: out std_logic
	);
end entity malloc;

architecture rtl of malloc is

signal cur_search_ptr					:	header;				--pointers

signal nstate,nnstate					: 	malloc_state_t;     --state signals

--control signals
signal hfront,hend							:	std_logic;
--I/O signals
signal s_size							:	size_t;
signal s_f_addr							:	integer;

--memory control signals
signal A_addr,B_addr					:	addr_t;
signal A_din,B_din,A_dout,B_dout		: 	data_t;
signal W_A,W_B							:	std_logic;

begin

--Input 

s_f_addr<=to_integer(unsigned(f_addr));

rounder:process(m_size)
begin
if m_size(0)='1' then
s_size<=unsigned(m_size)+1;
else
s_size<=unsigned(m_size);
end if;
end process;


--MEM
 MEM1:entity dpram_single_clk_256x32 port map
 (
	 clk 	=>	clk,
	 addr_a	=>	A_addr,
	 addr_b	=>	B_addr,
	 data_a	=>	A_din,
	 data_b	=>	B_din,
	 we_a	=>	W_A,
	 we_b	=>	W_B,
	 q_a	=>	A_dout,
	 q_b	=>	B_dout
 );

--control signals
	hfront<='1' when cur_search_ptr.h_prev=prev_err else '0';		--indication of reaching list front
	hend<='1' when cur_search_ptr.h_next=next_err else '0';
--FSM
FSM_proc:process
variable ptr					:	header;
variable new_header,old_header	:	slv(63 downto 0);
variable free_sel				:	slv(1 downto 0);
variable prev_size,next_size	:	size_t;

begin
wait until clk'EVENT and clk='1';

nstate<=nstate;

--done<='0';
W_A<='0';
W_B<='0';
case(nstate)is
----wait 2 cycles	
when mem_wait1=> nstate<=mem_wait2;
				 W_A<=W_A;
				 W_B<=W_B;
when mem_wait2=> nstate<=nnstate;
				 W_A<=W_A;
				 W_B<=W_B;
when idle=>	
			if en = '1' and  go='1' then
				done<='0';
				if op='0' then
				A_addr<=0;
				B_addr<=1;
				nstate<=mem_wait1;
				nnstate<=find;
				else
				A_addr<=s_f_addr-header_size;
				B_addr<=s_f_addr-header_size+1;
				nstate<=mem_wait1;
				nnstate<=init_free;
				end if;
			end if;
when find=>	
			ptr:=readHeader(A_dout,B_dout);	
			old_header:=update_oldHeader(ptr,s_size);
			cur_search_ptr<=ptr;
			
				if 	(ptr.free='1' and (unsigned(ptr.size)>=s_size)) then
					if (unsigned(ptr.size)<=s_size+header_size) then 		--just fit
						W_A<='1';
						A_din<=A_dout(31 downto 17)&'0'&A_dout(15 downto 0);
						
						done<='1';
						nstate<=mem_wait1;	
						nnstate<=idle;
					else
					W_A<='1';						--write
					W_B<='1';
					
					A_din<=old_header(63 downto 32);
					B_din<=old_header(31 downto 0);
					
					nstate<=mem_wait1;	
					nnstate<=alloc;
					end if;
					m_addr<=std_logic_vector(to_unsigned(ptr.data,16));
					
				else
					if ptr.h_next/=next_err then
						A_addr<=ptr.h_next;			--next block
						B_addr<=ptr.h_next+1;
						nstate<=mem_wait1;
						nnstate<=find;
					else							--error handle
						m_addr<=(others=>'0');
						nstate<=mem_wait1;
						nnstate<=idle;
						done<='1';
					end if;
				end if;				
when alloc=>	new_header:=create_newHeader(cur_search_ptr,s_size);
				
				W_A<='1';
				W_B<='1';
				A_addr<=to_integer(s_size+ptr.data);
				B_addr<=to_integer(s_size+ptr.data+addr_step);
				
				A_din<=new_header(63 downto 32);
				B_din<=new_header(31 downto 0);
				
				done<='1';
				nstate<=mem_wait1;
				nnstate<=idle;
when init_free=>
                ptr:=readHeader(A_dout,B_dout);
				cur_search_ptr<=ptr;
				A_addr<=ptr.h_prev;
				B_addr<=ptr.h_next;
				nstate<=mem_wait1;
				nnstate<=free;
------------------------------------------------------------------------------------------------------
----freesel 		case								strategy
----	00			notfree->free.->notfree				free current addr.
----	01			notfree->free.->free				free current addr.+link current to Nnext
----	10			free->free.->notfree				link prev to next
----	11			free->free.->free					link prev to Nnext
------------------------------------------------------------------------------------------------------				
when free=>		
			prev_size:=unsigned(A_dout(31 downto 17)&'0');
			next_size:=unsigned(B_dout(31 downto 17)&'0');

            if hfront='1' then
                    free_sel:='0'&B_dout(16);
			else
				if 	hend='1' then
					free_sel:=B_dout(16)&'0';
				else
					free_sel:=A_dout(16)&B_dout(16);
				end if;
			end if;
				
					if free_sel="00" then											--notfree->free->notfree
						W_A<='1';
						A_addr<=cur_search_ptr.data-header_size;
						A_din<=std_logic_vector((cur_search_ptr.size+1)&i2t(cur_search_ptr.data));
						done<='1';
						nstate<=mem_wait1;
						nnstate<=idle;
					
					elsif free_sel="01" then										--notfree->free->free
						--change current node size&status
						W_A<='1';
						A_addr<=cur_search_ptr.data-header_size;
						A_din<=std_logic_vector((cur_search_ptr.size+1+header_size+next_size)&i2t(cur_search_ptr.data));
						--check address after next
						B_addr<=cur_search_ptr.h_next+addr_step;
						
						nstate<=mem_wait1;
						nnstate<=free1;
					
					elsif free_sel="10" then							--free->free->notfree 	
						--change previous node size&status
						W_A<='1';
						A_addr<=cur_search_ptr.h_prev;
						A_din<=std_logic_vector((cur_search_ptr.size+1+header_size+prev_size))&A_dout(15 downto 0);
						B_addr<=cur_search_ptr.data-header_size+addr_step;
						nstate<=mem_wait1;
						nnstate<=free1;
					
					else														--free->free->free 		
						W_A<='1';
						A_addr<=cur_search_ptr.h_prev;
						A_din<=std_logic_vector((cur_search_ptr.size+1+header_size+header_size+prev_size+next_size))&A_dout(15 downto 0);
												
						B_addr<=cur_search_ptr.h_next+addr_step;
						nstate<=mem_wait1;
						nnstate<=free2;
					end if;
when free1=>	
				A_addr<=to_integer(unsigned(B_dout(15 downto 0)))+addr_step;
				B_addr<=to_integer(unsigned(B_dout(31 downto 16)))+addr_step;
		        nstate<=mem_wait1;
		        nnstate<=link;
when free2=>	
				A_addr<=cur_search_ptr.h_prev+addr_step;
				B_addr<=to_integer(unsigned(B_dout(31 downto 16)))+addr_step;
				nstate<=mem_wait1;
				nnstate<=link;
when link=>		
				if (A_addr/=prev_err+addr_step) then 
				W_A<='1';
				A_din<=std_logic_vector(i2t(B_addr-addr_step))&A_dout(15 downto 0);	--prev
				end if;
				
				if (B_addr/=next_err+addr_step) then
				W_B<='1';
				B_din<=B_dout(31 downto 16)&std_logic_vector(i2t(A_addr-addr_step));--next
				end if;
				done<='1';
				nstate<=mem_wait1;
				nnstate<=idle;
end case;

end process FSM_proc;

end architecture rtl;
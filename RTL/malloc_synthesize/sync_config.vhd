library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.all;

package config_pak is

--alias
ALIAS slv is std_logic_vector;
ALIAS usg is unsigned;
ALIAS sgn is signed;

--types
subtype addr_t is integer range 0 to 255;
subtype size_t is usg(15 downto 0);
subtype data_t is slv(31 downto 0);

type header is record
	size:	size_t;
	data:	addr_t;
	h_prev: addr_t;
	h_next: addr_t;
	free:	std_logic;
end record;

--constants
constant header_size: 	integer:= 2;
constant addr_step:		integer:= 1;
constant next_err:		integer:= 0;
constant prev_err:		integer:= 255;
constant null_val:		slv(31 downto 0):=(others=>'0');
--states
type malloc_state_t is (idle,find,alloc,init_free,free,free1,free2,mem_wait1,mem_wait2,link);

--utility functions
FUNCTION readHeader(A:slv(31 downto 0);B:slv(31 downto 0)) RETURN header;
FUNCTION create_newHeader(ptr:header; size:size_t) RETURN std_logic_vector;
FUNCTION update_oldHeader(ptr:header; size:size_t) RETURN std_logic_vector;
--type_conversion functions
FUNCTION b2l(b:BIT) return std_logic;	
FUNCTION i2t(i : integer) RETURN usg;
end package config_pak;


package body config_pak is
-----------------------------------------------------------------------------------------------------------
FUNCTION readHeader(A:slv(31 downto 0);B:slv(31 downto 0)) RETURN header IS
		VARIABLE curPtr:header;
	BEGIN
		curPtr.size:=unsigned(A(31 downto 17)&'0');
		curPtr.free:=A(16);
		curPtr.data:=to_integer(unsigned(A(15 downto 0)));
		curPtr.h_next:=to_integer(unsigned(B(31 downto 16)));
		curPtr.h_prev:=to_integer(unsigned(B(15 downto 0)));
		return(curPtr);
	END;
-----------------------------------------------------------------------------------------------------------

FUNCTION create_newHeader(ptr:header; size:size_t) RETURN std_logic_vector IS
	VARIABLE out_mem:std_logic_vector(63 downto 0);
BEGIN
	out_mem(63 downto 48):= std_logic_vector(ptr.size-size-header_size+1);
	out_mem(47 downto 32):= std_logic_vector(i2t(ptr.data)+size+header_size);
	out_mem(31 downto 16):= std_logic_vector(i2t(ptr.h_next));
	out_mem(15 downto 0) := std_logic_vector(i2t(ptr.data)-header_size);
	return out_mem;
END;

FUNCTION update_oldHeader(ptr:header; size:size_t) RETURN std_logic_vector IS
	VARIABLE out_mem:std_logic_vector(63 downto 0);
	BEGIN
		out_mem(63 downto 48):= std_logic_vector(size);
		out_mem(47 downto 32):= std_logic_vector(i2t(ptr.data));
		out_mem(31 downto 16):= std_logic_vector(i2t(ptr.data)+size);
		out_mem(15 downto 0) := std_logic_vector(i2t(ptr.h_prev));
		return out_mem;
	END;
-----------------------------------------------------------------------------------------------------------
FUNCTION b2l(b : BIT) RETURN std_logic IS
	BEGIN
		IF b = '0' THEN
			RETURN '0';
		END IF;
		RETURN '1';
	END FUNCTION;

FUNCTION i2t(i : integer) RETURN usg IS
	BEGIN
	return(to_unsigned(i,16));
	END FUNCTION;

end package body config_pak;


------------------------------------------------------------------
--Testbench for malloc blcok
--reads command.txt for input data

--vhdl test entity: malloc
--author: Zifan Guo
--version: 23/02/2014
------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
use work.config_pak.all;
USE work.all;
entity mem_tb is
end mem_tb;

architecture tb of mem_tb is
	signal clk,reset	:std_logic;
	signal A_addr,B_addr:			integer range 0 to 255;
	signal A_din,B_din,A_dout,B_dout: 	slv(31 downto 0);
	signal W_A,W_B:				std_logic;
begin	
	
--clock generation process	
clk_gen:process
begin
	clk<='0';
	wait for 50 ns;
	clk<='1';
	wait for 50 ns;
end process clk_gen;

--test entity
m1:entity work.dpram_single_clk_256x32
	port map(
		clk =>clk,
	addr_a=>A_addr,
	addr_b=>B_addr,
	data_a=>A_din,
	data_b=>B_din,
	we_a=>W_A,
	we_b=>W_B,
	q_a=>A_dout,
	q_b=>B_dout
	);


main: process 	
begin
	reset<='1';
	wait until clk'event and clk = '1';
	A_addr<=0;
	B_addr<=1;
	wait until clk'event and clk = '1';
	W_A<='1';
	W_B<='0';
	A_din<="00000000000000100000000000000010";
	B_din<="00000000000000100000000000001000";
	wait until clk'event and clk = '1';
	W_A<='0';
	W_B<='0';
	wait until clk'event and clk = '1';
	wait until clk'event and clk = '1';
	wait until clk'event and clk = '1';
	wait until clk'event and clk = '1';
	REPORT "test finished normally." SEVERITY failure;
end process main; 

end tb;
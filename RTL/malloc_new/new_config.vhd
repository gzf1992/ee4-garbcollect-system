library IEEE;
use IEEE.std_logic_1164.ALL;
use IEEE.numeric_std.ALL;
use work.all;

package config_pak is

--alias
ALIAS slv is std_logic_vector;
ALIAS usg is unsigned;
ALIAS sgn is signed;

--types
type ram_t is array (0 to 256) of std_logic_vector(31 downto 0);

subtype addr_t is std_logic_vector(15 downto 0);

type header is record
	size:	std_logic_vector(15 downto 0);
	data:	std_logic_vector(15 downto 0);
	h_prev: std_logic_vector(15 downto 0);
	h_next: std_logic_vector(15 downto 0);
	free:	std_logic;
end record;

--constants
constant header_size: 	integer:= 2;
constant addr_step:		integer:= 1;
constant next_err:		std_logic_vector:="0000000000000000";
constant prev_err:		std_logic_vector:="1111111111111111";

--states
type malloc_state_t is (idle,find,alloc,free,merge);

--utility functions
FUNCTION nextHeader(ptr:header; ram:ram_t ) RETURN header;
FUNCTION prevHeader(ptr:header; ram:ram_t ) RETURN header;
FUNCTION readHeader(addr:integer; ram:ram_t) RETURN header;
FUNCTION create_newHeader(ptr:header; size:unsigned(15 downto 0)) RETURN std_logic_vector;
FUNCTION update_oldHeader(ptr:header; size:unsigned(15 downto 0)) RETURN std_logic_vector;
--type_conversion functions
FUNCTION b2l(b:BIT) return std_logic;	
end package config_pak;


package body config_pak is

FUNCTION nextHeader(ptr:header; ram:ram_t) RETURN header IS
		VARIABLE nextptr:header;
	BEGIN		
		nextptr.size:=ram(to_integer(unsigned(ptr.h_next)))(31 downto 17)&'0';
		nextptr.free:=ram(to_integer(unsigned(ptr.h_next)))(16);
		nextptr.data:=ram(to_integer(unsigned(ptr.h_next)))(15 downto 0);	
		nextptr.h_next:=ram(to_integer(unsigned(ptr.h_next)+addr_step))(31 downto 16);
		nextptr.h_prev:=ram(to_integer(unsigned(ptr.h_next)+addr_step))(15 downto 0);	
		return(nextptr);
	END;
	
FUNCTION prevHeader(ptr:header; ram:ram_t) RETURN header IS
		VARIABLE prevptr:header;
	BEGIN
		
		prevptr.size:=ram(to_integer(unsigned(ptr.h_prev)))(31 downto 17)&'0';
		prevptr.free:=ram(to_integer(unsigned(ptr.h_prev)))(16);
		prevptr.data:=ram(to_integer(unsigned(ptr.h_prev)))(15 downto 0);	
		prevptr.h_next:=ram(to_integer(unsigned(ptr.h_prev)+addr_step))(31 downto 16);
		prevptr.h_prev:=ram(to_integer(unsigned(ptr.h_prev)+addr_step))(15 downto 0);
		return(prevptr);
	END;

FUNCTION readHeader(addr:integer; ram:ram_t) RETURN header IS
		VARIABLE curPtr:header;
	BEGIN
		curPtr.size:=ram(addr)(31 downto 17)&'0';
		curPtr.free:=ram(addr)(16);
		curPtr.data:=ram(addr)(15 downto 0);	
		curPtr.h_next:=ram(addr+addr_step)(31 downto 16);
		curPtr.h_prev:=ram(addr+addr_step)(15 downto 0);
		return(curPtr);
	END;
	
FUNCTION create_newHeader(ptr:header; size:unsigned(15 downto 0)) RETURN std_logic_vector IS
	
	VARIABLE out_mem:std_logic_vector(63 downto 0);
BEGIN
	out_mem(63 downto 48):= std_logic_vector(unsigned(ptr.size)-size-header_size+1);
	out_mem(47 downto 32):= std_logic_vector(unsigned(ptr.data)+size+header_size);
	out_mem(31 downto 16):= ptr.h_next;
	out_mem(15 downto 0) := std_logic_vector(unsigned(ptr.data)-header_size);
	return out_mem;
END;

FUNCTION update_oldHeader(ptr:header; size:unsigned(15 downto 0)) RETURN std_logic_vector IS
	VARIABLE out_mem:std_logic_vector(63 downto 0);
	BEGIN
		out_mem(63 downto 48):= std_logic_vector(size);
		out_mem(47 downto 32):= ptr.data;
		out_mem(31 downto 16):= std_logic_vector(unsigned(ptr.data)+size);
		out_mem(15 downto 0) := ptr.h_prev;
		return out_mem;
	END;


	
FUNCTION b2l(b : BIT) return std_logic is	
	BEGIN
		IF b = '0' THEN
			RETURN '0';
		END IF;
		RETURN '1';
	END FUNCTION;

end package body config_pak;

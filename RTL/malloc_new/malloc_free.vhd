library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.config_pak.all;
--USE work.txt_util.ALL;
entity malloc is
	port (
		clk : in  std_logic;
		en  : in  std_logic;
		req : in  std_logic;
		op  : in  std_logic;
		m_size: in  std_logic_vector(15 downto 0);
		f_addr:in std_logic_vector(15 downto 0);
		
		m_addr: out std_logic_vector(15 downto 0);
		done: out std_logic
	);
end entity malloc;

architecture clear of malloc is

signal ram: 		ram_t:=(0=>"11111111111111110000000000000010",1=>"00000000000000001111111111111111",others=>"00000000000000000000000000000000");                                   --internal ram

signal base_ptr,cur_search_ptr,prev_search_ptr:	header;			--pointers

signal state,nstate: malloc_state_t;                      --state signals

--control signals
signal found,hend,hfront:		 std_logic;

--I/O signals
signal s_size:		unsigned(15 downto 0);
signal s_addr:		slv(15 downto 0);
signal s_f_addr:	integer;
begin

	s_size<=unsigned(m_size);
	m_addr<=s_addr;
	s_f_addr<=to_integer(unsigned(f_addr));

--fix base pointer
	base_ptr<=readHeader(0,ram);
	
--control signals
	found<='1' when (cur_search_ptr.free='1' and (unsigned(cur_search_ptr.size)>s_size)) else '0';
	hend<='1' when cur_search_ptr.h_next=next_err else '0';		--indication of reaching list end
	hfront<='1' when cur_search_ptr.h_prev=prev_err else '0';	--indication of reaching list front
	
	
	FSM_process:process
	variable new_header,old_header	:std_logic_vector(63 downto 0);
	variable prev_addr,cur_addr,next_addr		:integer;
			
	begin
		wait until clk'EVENT and clk='1';
	
		state<=nstate;
		done<='0';

		case (nstate) is
			when idle=> 
			
						if en = '1' and  req='1' then
							if op='0' then
							nstate<=find;
							cur_search_ptr<=base_ptr;
							else 
							nstate<=free;
							cur_search_ptr<=readHeader(s_f_addr-2,ram);
							end if;
						end if;
			when find=> 
						if found ='0' then
							if hend/='1' then 
								nstate<=find;
								prev_search_ptr<=cur_search_ptr;
								cur_search_ptr<=nextHeader(cur_search_ptr,ram);
							else
								s_addr<=(others=>'0');
								nstate<=idle;
								done<='1';
							end if;
						else
						nstate<=alloc;
						s_addr<=cur_search_ptr.data;
						end if;						
			when alloc=>
						new_header:=create_newHeader(cur_search_ptr,s_size);
						old_header:=update_oldHeader(cur_search_ptr,s_size);
						cur_addr:=to_integer(unsigned(cur_search_ptr.data)-header_size);
						next_addr:=to_integer(unsigned(cur_search_ptr.data)+s_size);
						
						ram(next_addr)<=new_header(63 downto 32);
						ram(next_addr+addr_step)<=new_header(31 downto 0);
						ram(cur_addr)<=old_header(63 downto 32);
		     				ram(cur_addr+addr_step)<=old_header(31 downto 0);
		     				done<='1';
						if op='0' then
		     				nstate<=idle;
						else
						nstate<=free;
						end if;
			when free=>		prev_addr:=to_integer(unsigned(cur_search_ptr.h_prev));
						cur_addr:=to_integer(unsigned(cur_search_ptr.data)-header_size);
						next_addr:=to_integer(unsigned(cur_search_ptr.h_next));
						REPORT "prev: "&integer'image(prev_addr)&"cur:"&integer'image(cur_addr)&"next:"&integer'image(next_addr) SEVERITY note;
						if prevHeader(cur_search_ptr,ram).free='0' and nextHeader(cur_search_ptr,ram).free='0' then
							ram(cur_addr)(16)<='1';
						elsif prevHeader(cur_search_ptr,ram).free='0' then
							ram(cur_addr)(16)<='1';
							ram(cur_addr+addr_step)(31 downto 16)<=nextHeader(cur_search_ptr,ram).h_next;
						elsif nextHeader(cur_search_ptr,ram).free='0' then
							ram(prev_addr+addr_step)(31 downto 16)<=cur_search_ptr.h_next;
						else
							ram(prev_addr+addr_step)(31 downto 16)<=nextHeader(cur_search_ptr,ram).h_next;
						end if;
						done<='1';
						if op='0' then
		     				nstate<=find;
						else
						nstate<=idle;
						end if;
			when merge=>NULL;
		end case;
	end process;
	
end architecture clear;

------------------------------------------------------------------
--Testbench for malloc blcok
--reads command.txt for input data

--vhdl test entity: malloc
--author: Zifan Guo
--version: 23/02/2014
------------------------------------------------------------------

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
USE std.textio.ALL;
USE work.txt_util.ALL;
USE work.config_pak.ALL;

entity malloc_tb is
end malloc_tb;

architecture tb of malloc_tb is
	signal clk,reset,en,operation,done_i,request	:std_logic;
	signal size_in,result,f_addr_i :std_logic_vector(15 downto 0);
begin	
	
--clock generation process	
clk_gen:process
begin
	clk<='0';
	wait for 50 ns;
	clk<='1';
	wait for 50 ns;
end process clk_gen;

--test entity
m1:entity work.malloc
	port map(
		clk 	=>clk ,
		en	=> en,
		op 	=> operation,
		req	=>request,
		m_size 	=>size_in,
		f_addr	=>f_addr_i,
		m_addr 	=>result,
		done	=>done_i
	);


main: process 	
	file f 				:TEXT open read_mode is "command.txt";
	variable buf 		:LINE;
	variable n			:integer;--line counter
	variable incorrect_result : integer;
	
	variable op			:bit;
	variable size		:bit_vector(15 downto 0);
	variable free_addr		:bit_vector(15 downto 0);
	variable nominal_result :bit_vector(15 downto 0);
begin
	reset<='1';
	wait until clk'event and clk = '1';
	reset <= '0';
	en<='1';
	
	n:=1;
	incorrect_result:=0;
	--------------------------------------------
	--read line
	
	while not endfile(f) loop
		wait until clk'event and clk='1';
		
		readline(f,buf);
		If buf'LENGTH = 0 THEN
			REPORT "skipping line: " & INTEGER'IMAGE(n) SEVERITY note;
		ELSE
			REPORT "Reading input line:" & INTEGER'IMAGE(n) SEVERITY note;
				request<='1';
				read(buf, op);
				operation<=b2l(op);
				
				read(buf,size);
				size_in<=to_stdlogicvector(size);
				
				read(buf,free_addr);
				f_addr_i<=to_stdlogicvector(free_addr);

				read(buf,nominal_result);
							
			REPORT "Nominal: " & str(to_stdlogicvector(nominal_result)) & "calculated " & str(result) SEVERITY note;
		END IF;
	wait until done_i'event and done_i='1';	
	end loop;

	REPORT "test finished normally." SEVERITY failure;
end process main; 

end tb;
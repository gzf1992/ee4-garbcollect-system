#include "malloc_functions.h"
#include <iostream>
#define DEBUG1
h_list sys_malloc(size_t size)
{
	size_t rounded_size;
	h_list cur_ptr;

	rounded_size = rounder(size);
	cur_ptr = find_space(base_ptr, rounded_size);
	return cur_ptr;
};

h_list find_space(h_list ptr,size_t size)
{	
	h_list new_block;
	
	if (ptr->free==1 && ptr->size >= size)
		if (ptr->size <= size + HEADER_SIZE)
			//Just fit
		{
			ptr->free = 0;
		}
		else
			//Splitting
		{
			new_block = new header;
			new_block->free = 1;
			new_block->size = ptr->size - size - HEADER_SIZE;
			new_block->data = ptr->data + size + HEADER_SIZE;
			new_block->h_next = ptr->h_next;
			new_block->h_prev = ptr;

			ptr->free = 0;
			ptr->size = size;
			ptr->h_next = new_block;
			return ptr;
		}	
		
	else if (ptr->h_next == NULL)
		{
			return NULL;
		}
	else
		{
			find_space(ptr->h_next,size);	
		}
};

int sys_free(h_list free_addr)
{
	int sel;
	h_list ptr;

	if (free_addr->free != 1)//correct operation
	{
		if (free_addr->h_prev == NULL)
			if (free_addr->h_next == NULL)
				sel = 0;
			else
				sel = free_addr->h_next->free;
		else
			if (free_addr->h_next == NULL)
				sel = 2 * (free_addr->h_prev->free);
			else
				sel = 2 * (free_addr->h_prev->free) + (free_addr->h_next->free);

		switch (sel)
		{
		case 0:
			free_addr->free = 1;
			break;

		case 1:
			free_addr->free = 1;
			free_addr->size = free_addr->size + free_addr->h_next->size + HEADER_SIZE;
			
			ptr = free_addr->h_next;

			free_addr->h_next = ptr->h_next;
			if (ptr->h_next != NULL)
			{
				ptr->h_next->h_prev = free_addr;
			}
				free(ptr);
			break;

		case 2:
				free_addr->free = 1;
				free_addr->h_prev->size = free_addr->h_prev->size + free_addr->size + HEADER_SIZE;
				free_addr->h_prev->h_next = free_addr->h_next;
				free_addr->h_next->h_prev = free_addr->h_prev;
				free(free_addr);
			break;

		case 3:
			free_addr->free = 1;
			free_addr->h_prev->size = free_addr->h_prev->size + free_addr->size + free_addr->h_next->size + 2 * HEADER_SIZE;


			ptr = free_addr->h_next;
			free_addr->h_prev->h_next = ptr->h_next;
			if (ptr->h_next != NULL)
			{
				ptr->h_next->h_prev = free_addr->h_prev;
			}

			free(free_addr);
			free(ptr);
			break;
		default:	NULL;
			break;
			
		}
#ifdef DEBUG1
		int count = 0;

		for (ptr = base_ptr; ptr->h_next != NULL; ptr = ptr->h_next)
		{
			count++;
			if (ptr->free == 1 && ptr->h_next->free == 1)
			{
				cout << "fault" << endl;
			}
		}
#endif // DEBUG1
		return 1;
	}
	else
	{ 
		return 0;
	}
		
};

void print_nodes(h_list base)
{
	h_list print_ptr;
	for (print_ptr = base; print_ptr->h_next != NULL; print_ptr = print_ptr->h_next)
	{
		cout <<"status: "<<print_ptr->free<<" size: "<<print_ptr->size<<" address "<< print_ptr->data << endl;
	}
	cout << "status: " << print_ptr->free << " size: " << print_ptr->size << " address " << print_ptr->data << endl;
};

void destroy_list(h_list list)
{	
	if (list->h_next != NULL)
		destroy_list(list->h_next);
	else
	{	if (list->h_prev!=NULL)
		{
			list->h_prev->h_next = list->h_next;
		}
		
		free(list);
	}
};

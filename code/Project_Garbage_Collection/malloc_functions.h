#include <cstdlib>
#include <bitset>
#include <cstdint>
#define ADDR_DEPTH 32
#define DATA_DEPTH 32
//#define ADDR_STEP 4
#define HEADER_SIZE 16
#define MEM_SIZE	1024*1024*512

using namespace std;

typedef uint32_t u32;

struct header
{
	int free;
	size_t size;	//u64
	header * h_next;
	u32 data;
	header * h_prev;
};
typedef header * h_list;

extern h_list base_ptr;
//utility
inline size_t rounder(size_t input_size)
{
	int remainder;
	remainder = input_size % 4;
	if (remainder == 0) 
		return input_size;
	else
		return input_size + 4 -remainder;
};

//malloc/free
h_list sys_malloc(size_t);
h_list find_space(h_list, size_t);
int sys_free(h_list);

//Debug functions
void print_nodes(h_list);
void destroy_list(h_list);
//Testing functions
void random_test(void);


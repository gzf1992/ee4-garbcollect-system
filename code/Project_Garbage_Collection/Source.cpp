#include <cstdlib>
#include <stdio.h>
#include <iostream>
#include <bitset>
#include <fstream>
#include "malloc_functions.h"

#define RANDOM_TEST

#ifdef RANDOM_TEST
#include <ctime>
#include <cstdint>
#include <random>
#include <iomanip>
#include <string>
#include <map>
#include <algorithm>
#define NUM_ITER	50
#define LAMBDA	0.01  
#endif // RANDOM_TEST

#define OUTPUTFILE "output.txt"
#define DEBUG1

typedef uint32_t u32;
typedef struct
{
	int size;
	h_list addr;
	int lifetime;
}object_t;
using namespace std;

h_list base_ptr;
void init_mem(void);

object_t * OBJ = (object_t *)malloc(NUM_ITER*sizeof(object_t));

int main()
{	
	
//-------------------------------------
//Initialsations
//-------------------------------------
	init_mem();
//--------------------------------------
//exponential random fragmentation test
//--------------------------------------
#ifdef RANDOM_TEST
	random_device rd;
	mt19937	gen(rd());
	// if particles decay once per second on average,
	// how much time, in seconds, until the next one?
	std::exponential_distribution<> d(LAMBDA);

	int max_lifetime = -1;

	//Testing set generation
	for (int i = 0; i < NUM_ITER; i++)
	{
		OBJ[i].size = 1000 * d(gen);
		OBJ[i].lifetime = d(gen) + 1;
		OBJ[i].addr = NULL;
		if (OBJ[i].lifetime > max_lifetime)
			max_lifetime = OBJ[i].lifetime;
	}

#ifdef DEBUG

	for (int i = 0; i < NUM_ITER; i++)
	{
		if (f.is_open())
		{
			f << OBJ[i].size << " " << OBJ[i].lifetime << " " << (int)OBJ[i].addr << endl;
		}
	}
#endif // DEBUG

	for (int j = 0; j < max_lifetime + NUM_ITER; j++)
	{
		if (j < NUM_ITER)
		{
			OBJ[j].addr = sys_malloc(OBJ[j].size);
		}
		//print_nodes(base_ptr);
		for (int k = 0; k <= min(j, NUM_ITER - 1); k++)
		{
			if (OBJ[k].lifetime == 0)
			{	
				cout << endl;
				sys_free(OBJ[k].addr);
			}
			if (OBJ[k].lifetime >= 0)
			OBJ[k].lifetime--;
		}
		//print_nodes(base_ptr);
	}
	free(OBJ);
#endif // RANDOM_TEST

//	ofstream f(OUTPUTFILE);

//	f.close();
	print_nodes(base_ptr);
	destroy_list(base_ptr);
	return 0;
}

#ifdef DEBUG
std::map<int, int> hist;

for (int n = 0; n<10000; ++n) {
	++hist[2 * d(gen)];
}

for (auto p : hist) {
	std::cout << std::fixed << std::setprecision(1)
		<< p.first / 2.0 << '-' << (p.first + 1) / 2.0 <<
		' ' << std::string(p.second / 100, '*') << '\n';
}
#endif // DEBUG

void init_mem(void)
{
	base_ptr = new header;
	base_ptr->free = 1;
	base_ptr->size = MEM_SIZE - HEADER_SIZE;
	base_ptr->h_next = NULL;
	base_ptr->data = HEADER_SIZE;
	base_ptr->h_prev = NULL;
};
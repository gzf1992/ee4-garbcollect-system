#include <ctime>
#include <iostream>
#include <fstream>
#include <cstdint>
#include <random>

#include <iomanip>
#include <string>
#include <map>
#include <algorithm>
#define NUM_ITER	10000
#define OUTPUTFILE "output.txt"
#define LAMBDA	0.01
//#define DEBUG

typedef uint32_t u32;
typedef struct
{
	int size;
	void * addr;
	int lifetime;
}object_t;
using namespace std;


object_t * OBJ = (object_t *)malloc(NUM_ITER*sizeof(object_t));

int main()
{
	random_device rd;
	mt19937	gen(rd());
	
	// if particles decay once per second on average,
	// how much time, in seconds, until the next one?
	std::exponential_distribution<> d(LAMBDA);

#ifndef DEBUG
	int max_lifetime=-1;
	for (int i = 0; i < NUM_ITER; i++)
	{
		OBJ[i].size = 1000 * d(gen);
		OBJ[i].lifetime = d(gen) + 1;
		OBJ[i].addr = NULL;
		if (OBJ[i].lifetime > max_lifetime)
			max_lifetime = OBJ[i].lifetime;
	}
	ofstream f(OUTPUTFILE);
	for (int i = 0; i < NUM_ITER; i++)
	{
		if (f.is_open())
		{
			f << OBJ[i].size << " " << OBJ[i].lifetime << " " <<(int)OBJ[i].addr<< endl;
		}
	}

	for (int j = 0; j < max_lifetime + NUM_ITER; j++)
	{
		if (j < NUM_ITER)
		{
			OBJ[j].addr = malloc(OBJ[j].size);
		}
		for (int k = 0; k <= min(j, NUM_ITER-1); k++)
		{
			if (OBJ[k].lifetime == 0)
			{
				free(OBJ[k].addr);
			}
			OBJ[k].lifetime--;
		}
	}
	f.close();
	free(OBJ);
	return 0;  
#endif // !DEBUG


#ifdef DEBUG
	std::map<int, int> hist;

	for(int n=0; n<10000; ++n) {
		++hist[2*d(gen)];
	}

	for (auto p : hist) {
		std::cout << std::fixed << std::setprecision(1)
			<< p.first / 2.0 << '-' << (p.first + 1) / 2.0 <<
			' ' << std::string(p.second/100, '*') << '\n';
	}
#endif // DEBUG

};